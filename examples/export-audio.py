#!/usr/bin/env python

"""
Copyright (C) 2012 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os, struct, sys
import Image

from replay import ReplayReader

def acorn_ul_to_sun_ul(data):

    new_data = ""
    for i in data:
    
        j = ~((ord(i) >> 1) | (ord(i) << 7)) & 0xff
        new_data += chr(j)
    
    return new_data

if __name__ == "__main__":

    if len(sys.argv) != 3:
    
        sys.stderr.write("Usage: %s <Replay file> <output file>\n" % sys.argv[0])
        sys.exit(1)
    
    replay_file = sys.argv[1]
    output_file = sys.argv[2]
    
    reader = ReplayReader()
    movie = reader.read(replay_file)
    movie.read_chunks()
    
    if not movie.chunk_info:
        print "No chunks found."
        sys.exit()
    
    mu_law = "-law" in str(movie.sound_precision)
    exponential = "exponential" in str(movie.sound_precision)
    unsigned = "unsigned" in str(movie.sound_precision)
    
    f = open(movie.path, "rb")
    out_f = open(output_file, "wb")
    
    for offset, video_size, audio_size in movie.chunk_info:
    
        f.seek(offset + video_size, 0)
        data = f.read(audio_size)
        
        if mu_law or exponential:
            data = acorn_ul_to_sun_ul(data)
        
        out_f.write(data)
    
    out_f.close()
    f.close()
    
    if mu_law or exponential:
        print "Try playing the audio file using the following command:"
        print "play -t ul -c 1 -r %i %s" % (int(movie.audio_replay_rate), output_file)
    else:
        print "Try playing the audio file using the following command:"
        print "play -t raw -c %i -r %i -e %s -b %i %s" % (movie.sound_channels,
            movie.audio_replay_rate, {True: "unsigned", False: "signed"}[unsigned],
            movie.sound_precision, output_file)
    
    sys.exit()
