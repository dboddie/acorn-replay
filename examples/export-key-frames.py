#!/usr/bin/env python

"""
Copyright (C) 2012 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os, struct, sys
from PyQt4.QtGui import QImage

from replay import ReplayReader, yuv555_to_rgb

if __name__ == "__main__":

    if len(sys.argv) != 3:
    
        sys.stderr.write("Usage: %s <Replay file> <output directory>\n" % sys.argv[0])
        sys.exit(1)
    
    replay_file = sys.argv[1]
    output_dir = sys.argv[2]
    
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    
    reader = ReplayReader()
    movie = reader.read(replay_file)
    movie.read_key_frames()
    
    if not movie.key_frames:
        print "No key frames found."
        sys.exit()
    
    f = 0
    format = "%%0%ii.png" % len(str(len(movie.key_frames)))
    
    for frame in movie.key_frames:
    
        if "YUV" in str(movie.depth):
            rgb = yuv555_to_rgb(frame)
            image_format = QImage.Format_RGB32
            swap_rgb = False
        elif int(movie.depth) == 16:
            rgb = frame
            image_format = QImage.Format_RGB555
            swap_rgb = True
        else:
            rgb = frame
            image_format = QImage.Format_RGB32
            swap_rgb = False
        
        image = QImage(rgb, int(movie.width), int(movie.height), image_format)
        if swap_rgb:
            image = image.rgbSwapped()
        image.save(os.path.join(output_dir, format % f))
        
        f += 1
        sys.stdout.write("\rWritten frame %i/%i" % (f, len(movie.key_frames)))
        sys.stdout.flush()
    
    print
    sys.exit()
