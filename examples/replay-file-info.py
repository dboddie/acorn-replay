#!/usr/bin/env python

"""
Copyright (C) 2012 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os, struct, sys

from replay import ReplayReader

# From the AE7doc.txt file:

formats = {
    "video": {
         0: "no video",
         1: "'Moving Lines' compression",
         2: "16bpp uncompressed",
         3: "10bpp YUV chroma horiz subsampled by 2",
         4: "8bpp uncompressed [pixel depth=8]",
         5: "8bpp YUV chroma horiz and vert subsampled by 2",
         6: "6bpp YUV chroma horiz and vert subsampled by 4",
         7: "'Moving Blocks' compression",
         8: "24bpp uncompressed",
         9: "16bpp YUV chroma horiz subsampled by 2 (YYUV8)    (uses YUV)",
        10: "12bpp YUV chroma horiz and vert subsampled by 2   (uses YUV)",
        11: "9bpp YUV chroma horiz and vert subsampled by 4    (uses 6Y5UV)",
        12: "ARMovie file containing pointer to real MPEG movie",
        13: "MPEG video data in ARMovie file",
        14: "Ultimotion",
        15: "'indirect' video",
        16: "12bpp YUV chroma horiz and vert subsampled by 2   (uses 6Y5UV)",
        17: "'Moving Blocks HQ' compression                    (uses YUV)",
        18: "h263                                              (uses 6Y5UV)",
        19: "'Super Moving Blocks' compression                 (uses 6Y5UV)",
        20: "'Moving Blocks Beta' compression                  (uses 6Y6UV)",
        21: "16bpp YUV chroma horiz subsampled by 2 (YUYV8)    (uses 6Y5UV)",
        22: "12bpp YY8UVd4 chroma horiz subsampled by 2        (uses 6Y5UV)",
        23: "11bpp 6Y6Y5U5V chroma horiz subsampled by 2       (uses 6Y5UV)",
        24: "8.25bpp 6Y5UV chroma horiz and vert subsamp by 2  (uses 6Y5UV)",
        25: "6bpp YYYYd4UVd4 chroma horiz and vert subsamp by 2(uses 6Y6UV)"
        },
    
    "audio": {
        0: "no audio",
        1: "basic audio",
        2: "'indirect' audio"
        }
    }

format_ranges = [
    (100, 199, "EIDOS"),
    (200, 299, "Irlam instruments"),
    (300, 399, "Wild Vision"),
    (400, 499, "Aspex Software"),
    (500, 599, "Iota"),
    (600, 699, "Warm Silence Software"),
    (800, 809, "Henrik Bjerregaard Pedersen"),
    (810, 899, "Small Users of compression numbers"),
    (900, 999, "Innovative Media Solutions")
    ]

def get_video_format(format):

    number = int(format)
    
    try:
        return formats["video"][number]
    except KeyError:
        for begin, end, name in format_ranges:
            if begin <= number <= end:
                return name
    
    return "Unknown"

def get_audio_format(format):

    number = int(format)
    
    try:
        return formats["audio"][number]
    except KeyError:
        return "Unknown"


if __name__ == "__main__":

    if len(sys.argv) != 2:
    
        sys.stderr.write("Usage: %s <Replay file>\n" % sys.argv[0])
        sys.exit(1)
    
    replay_file = sys.argv[1]
    
    reader = ReplayReader()
    movie = reader.read(replay_file)
    
    print "Name:", movie.name
    print "Copyright:", movie.copyright
    print "Author:", movie.author
    print
    print "Video format:", get_video_format(movie.video_compression_format)
    print "Width:", movie.width
    print "Height:", movie.height
    print "Depth:", movie.depth
    print "Replay rate:", movie.frames_per_second
    print
    print "Audio format:", movie.audio_compression_format
    print "Replay rate:", movie.audio_replay_rate
    print "Channels:", movie.sound_channels
    print "Precision:", movie.sound_precision
    print
    print "Frames per chunk:", movie.frames_per_chunk
    print "Number of chunks:", movie.number_of_chunks
    print "Even chunk size:", movie.even_chunk_size
    print "Odd chunk size:", movie.odd_chunk_size
    print "Chunk catalogue offset:", movie.chunk_catalogue_offset
    print "Helpful sprite offset:", movie.helpful_sprite_offset
    print "Helpful sprite info size:", movie.helpful_sprite_info_size
    print "Key frames offset:", movie.key_frames_offset
    
    sys.exit()
