"""
Copyright (C) 2012 David Boddie <david@boddie.org.uk>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import struct

def yuv555_to_rgb(data):

    new = ""
    
    i = 0
    while i < len(data):
    
        yuv = struct.unpack("<H", data[i:i+2])[0]
        
        # http://www.equasys.de/colorconversion.html
        y = (yuv & 0x1f)/31.0
        
        u = ((yuv >> 5) & 0x1f)
        if u >= 16:
            u = u - 32
        u = u/16.0 * 0.615
        
        v = ((yuv >> 10) & 0x1f)
        if v >= 16:
            v = v - 32
        v = v/16.0 * 0.436
        
        # http://en.wikipedia.org/wiki/YUV#Conversion_to.2Ffrom_RGB
        r = y + (1.13983 * v)
        g = y - (0.39465 * u) - (0.58060 * v)
        b = y + (2.03211 * u)
        
        r = min(max(0, int(r * 255)), 255)
        g = min(max(0, int(g * 255)), 255)
        b = min(max(0, int(b * 255)), 255)
        
        new += struct.pack("<BBBB", b, g, r, 255)
        i += 2
    
    return new

def yuv_to_rgb(Y, U, V):

    new = ""
    
    i = 0
    l = len(Y)
    while i < l:
    
        y = Y[i]
        u = U[i]
        v = V[i]
        
        y = y / 31.0
        
        if u >= 16:
            u = u - 32
        u = u/16.0 * 0.615
        
        if v >= 16:
            v = v - 32
        v = v/16.0 * 0.436
        
        r = y + (1.13983 * v)
        g = y - (0.39465 * u) - (0.58060 * v)
        b = y + (2.03211 * u)
        
        r = min(max(0, int(r * 255)), 255)
        g = min(max(0, int(g * 255)), 255)
        b = min(max(0, int(b * 255)), 255)
        
        new += struct.pack("<BBBB", b, g, r, 255)
        i += 1
    
    return new


class Common:

    def _read(self, format):
    
        size = struct.calcsize(format)
        items = struct.unpack(format, self.f.read(size))
        
        if format[0] in "!<>":
            format = format[1:]
        
        if len(format) == 1:
            return items[0]
        else:
            return items
    
    def _read_decimal(self):
    
        s = self._read_string()
        pieces = s.split()
        
        if len(pieces) >= 1:
        
            value = float(pieces[0].replace("K", "000"))
            if len(pieces) > 1:
                return Decimal(value, " ".join(pieces[1:]))
            else:
                return Decimal(value)
        else:
            return Decimal(0)
    
    def _read_string(self):
    
        s = ""
        while True:
        
            c = self.f.read(1)
            if ord(c) >= 32:
                s += c
            else:
                break
        
        return s.strip()


class ReplayReader(Common):

    def read(self, path):
    
        self.f = open(path, "rb")
        
        # Check the file type.
        if self._read_string() != "ARMovie":
            raise IOError, "Not a Replay file: %s" % path
        
        movie = Movie(path)
        
        movie.name = self._read_string()
        movie.copyright = self._read_string()
        movie.author = self._read_string()
        
        movie.video_compression_format = self._read_decimal()
        
        movie.width = self._read_decimal()
        movie.height = self._read_decimal()
        movie.depth = self._read_decimal()
        movie.frames_per_second = self._read_decimal()
        
        movie.audio_compression_format = self._read_decimal()
        
        movie.audio_replay_rate = self._read_decimal()
        movie.sound_channels = self._read_decimal()
        movie.sound_precision = self._read_decimal()
        
        movie.frames_per_chunk = self._read_decimal()
        movie.number_of_chunks = self._read_decimal()
        movie.even_chunk_size = self._read_decimal()
        movie.odd_chunk_size = self._read_decimal()
        movie.chunk_catalogue_offset = self._read_decimal()
        movie.helpful_sprite_offset = self._read_decimal()
        movie.helpful_sprite_info_size = self._read_decimal()
        movie.key_frames_offset = self._read_decimal()
        
        self.f.seek(int(movie.helpful_sprite_offset), 0)
        movie.sprite = self.f.read(int(movie.helpful_sprite_info_size))
        
        self.f.close()
        return movie


class Decimal:

    def __init__(self, value, comment = ""):
    
        self.value = value
        self.comment = comment
    
    def __repr__(self):
    
        if self.comment:
            return "%i (%s)" % (self.value, self.comment)
        else:
            return "%i" % self.value
    
    def __int__(self):
    
        return int(self.value)


class Movie(Common):

    def __init__(self, path):
    
        self.path = path
    
    def read_chunks(self):
    
        self.f = open(self.path, "rb")
        self.f.seek(int(self.chunk_catalogue_offset), 0)
        
        self.chunk_info = []
        i = 0
        while i <= self.number_of_chunks.value:
        
            line = self._read_string()
            file_offset, rest = line.split(",")
            video_size, audio_size = rest.split(";")
            
            self.chunk_info.append((int(file_offset.strip()),
                                     int(video_size.strip()),
                                     int(audio_size.strip())))
            i += 1
        
        self.f.close()
    
    def read_key_frames(self):
    
        self.f = open(self.path, "rb")
        self.key_frames = []
        
        # Check for a zero or negative offset, though only zero is officially
        # documented as meaning "no keys".
        if self.key_frames_offset.value > 0:
        
            self.f.seek(int(self.key_frames_offset), 0)
            frame_size = int(self.width.value * self.height.value * self.depth.value/8)
            
            i = 0
            while i < self.number_of_chunks.value:
            
                self.key_frames.append(self.f.read(frame_size))
                i += 1
        
        self.f.close()
    
    def read_frames(self, index):
    
        self.read_key_frames()
        self.read_chunks()
        
        yuv = self.key_frames[index]
        offset, video_size, audio_size = self.chunk_info[index]
        
        f = open(self.path, "rb")
        f.seek(offset, 0)
        video_data = f.read(video_size)
        audio_data = f.read(audio_size)
        f.close()
        
        return Frames(self, video_data, audio_data, yuv)


class Frames:

    temporal_offsets = {
        1: [(-1,-1), (0,-1), (1,-1), (-1,0), (1,0), (-1,1), (0,1), (1,1)],
        2: [(-2,-2), (-1,-2), (0,-2), (1,-2), (2,-2), (-2,-1), (2,-1), (-2,0),
            (2,0), (-2,1), (2,1), (-2,2), (-1,2), (0,2), (1,2), (2,2)],
        3: [(-4,-4), (-3,-4), (-2,-4), (-1,-4), (0,-4), (1,-4), (2,-4), (3,-4),
            (4,-4), (-4,-3), (4,-3), (-4,2), (4,2), (-4,1), (4,1), (-4,0),
            (4,0), (-4,1), (4,1), (-4,2), (4,2), (-4,3), (4,3), (-4,4),
            (-3,4), (-2,4), (-1,4), (0,4), (1,4), (2,4), (3,4), (4,4),
            (-3,-3), (-2,-3), (-1,-3), (0,-3), (1,-3), (2,-3), (3,-3), (-3,-2),
            (3,-2), (-3,-1), (3,-1), (-3,0), (3,0), (-3,1), (3,1), (-3,2),
            (3,2), (-3,3), (-2,3), (-1,3), (0,3), (1,3), (2,3), (3,3)]
        }
    
    spatial_offsets = {
        4: [(-2,-4), (-1,-4), (0,-4), (1,-4), (2,-4), (-4,0), (-4,-1), (-4,-2)],
        2: [(-2,-2), (-1,-2), (-2,-1), (0,-2), (1,-2), (2,-2), (-2,0), (-3,0)]
        }
    
    def __init__(self, movie, video_data, audio_data, YUV_data):
    
        self.movie = movie
        self.width = int(movie.width)
        self.height = int(movie.height)
        self.video_data = video_data
        self.audio_data = audio_data
        
        self.Y = []
        self.U = []
        self.V = []
        
        i = 0
        l = len(YUV_data)
        while i < l:
            yuv = struct.unpack("<H", YUV_data[i:i+2])[0]
            self.Y.append(yuv & 0x1f)
            self.U.append((yuv >> 5) & 0x1f)
            self.V.append((yuv >> 10) & 0x1f)
            i += 2
    
    def clear_YUV(self):
    
        self.Y = [0] * self.width * self.height
        self.U = [0] * self.width * self.height
        self.V = [0] * self.width * self.height
    
    def read_value(self, bit_offset):
    
        offset = bit_offset / 8
        r = bit_offset % 8
        mask = (0x1f << r)
        
        # ------mm mmm-----
        #             <-r->
        data = (ord(self.video_data[offset]) & mask) >> r
        
        if r > 3:
            mask = mask >> 8
            data = data | ((ord(self.video_data[offset + 1]) & mask) << (8 - r))
        
        return bit_offset + 5, data
    
    def read_var_value(self, bit_offset, mask, bits):
    
        offset = bit_offset / 8
        r = bit_offset % 8
        mask = mask << r
        
        # Shift the value to the rightmost place.
        data = (ord(self.video_data[offset]) & mask) >> r
        # Shift the mask ready for the next byte.
        mask = mask >> 8
        # Adjust the shift offset to point to the end of the current value.
        r = 8 - r
        
        while mask != 0:
        
            offset += 1
            data |= ((ord(self.video_data[offset]) & mask) << r)
            # Shift the mask and update the shift offset.
            mask = mask >> 8
            r += 8
        
        return bit_offset + bits, data
    
    def read_bit(self, bit_offset):
    
        offset = bit_offset / 8
        r = bit_offset % 8
        b = ord(self.video_data[offset]) & (1 << r)
        return bit_offset + 1, b != 0
    
    def read_pair(self, bit_offset):
    
        offset = bit_offset / 8
        r = bit_offset % 8
        b = (ord(self.video_data[offset]) >> r) & 0x3
        if r == 7:
            b |= (ord(self.video_data[offset + 1]) & 1) << 1
        
        return bit_offset + 2, b
    
    def copy_YUVs(self, frame0, x0, y0, frame1, x1, y1, size):
    
        Y0, U0, V0 = frame0
        Y1, U1, V1 = frame1
        
        i = 0
        while i < size:
        
            j = 0
            while j < size:
            
                k0 = ((y0 + i) * self.width) + x0 + j
                k1 = ((y1 + i) * self.width) + x1 + j
                Y1[k1] = Y0[k0]
                U1[k1] = U0[k0]
                V1[k1] = V0[k0]
                j += 1
            
            i += 1
    
    def set_YUVs(self, bit_offset, x, y, size):
    
        i = 0
        while i < size:
        
            j = 0
            while j < size:
            
                bit_offset, Y = self.read_value(bit_offset)
                self.Y[((y + i) * self.width) + x + j] = Y
                j += 1
            
            i += 1
        
        bit_offset, U = self.read_value(bit_offset)
        bit_offset, V = self.read_value(bit_offset)
        
        i = 0
        while i < size:
        
            j = 0
            while j < size:
            
                k = ((y + i) * self.width) + x + j
                self.U[k] = U
                self.V[k] = V
                j += 1
            
            i += 1
        
        return bit_offset
    
    def move_piece(self, bit_offset, x, y, size):
    
        bit_offset, v = self.read_pair(bit_offset)
        
        # Prepare for a temporal copy by default.
        frame0 = self.prev_Y, self.prev_U, self.prev_V
        frame1 = self.Y, self.U, self.V
        
        if v == 0:
            # Temporal copy from same place
            self.copy_YUVs(frame0, x, y, frame1, x, y, size)
            return bit_offset
        elif v == 2:
            # Temporal copy +- 1
            bit_offset, l = self.read_var_value(bit_offset, 0x7, 3)
            dx, dy = self.temporal_offsets[1][l]
        elif v == 1:
            # Temporal copy +- 2
            bit_offset, l = self.read_var_value(bit_offset, 0xf, 4)
            dx, dy = self.temporal_offsets[2][l]
        else:
            bit_offset, l = self.read_var_value(bit_offset, 0x3f, 6)
            
            if l < 56:
                # Temporal copy +- 3
                # Temporal copy +- 4
                dx, dy = self.temporal_offsets[3][l]
            else:
                # Spatial copy
                dx, dy = self.spatial_offsets[size][l - 56]
                frame0 = frame1
        
        try:
            self.copy_YUVs(frame0, x + dx, y + dy, frame1, x, y, size)
        except:
            print l, x, y, dx, dy
            raise
        return bit_offset
    
    def read_block(self, bit_offset, x, y):
    
        bit_offset, b = self.read_bit(bit_offset)
        
        if b:
            # 1 -> (1) 4 x 4 data
            bit_offset = self.set_YUVs(bit_offset, x, y, 4)
            return bit_offset
        
        else:
            # 0 -> (0) move or subdivide
            
            bit_offset, b = self.read_bit(bit_offset)
            if b:
                # 1 -> (01) four 2 x 2 cases, each with a bit describing data
                #           or a move operation
                
                data = []
                
                i = 0
                while i < 4:
                
                    j = 0
                    while j < 4:
                    
                        bit_offset, b = self.read_bit(bit_offset)
                        if b:
                            # 1 -> 2 x 2 data
                            bit_offset = self.set_YUVs(bit_offset, x + j, y + i, 2)
                            data.append(y)
                        
                        else:
                            # 0 -> 2 x 2 move
                            bit_offset = self.move_piece(bit_offset, x + j, y + i, 2)
                        
                        j += 2
                    i += 2
            else:
                # 0 -> (00) move 4 x 4
                bit_offset = self.move_piece(bit_offset, x, y, 4)
        
        return bit_offset
    
    def read_frame(self, bit_offset = 0):
    
        self.prev_Y = self.Y[:]
        self.prev_U = self.U[:]
        self.prev_V = self.V[:]
        
        if int(self.movie.video_compression_format) == 7:
        
            # Moving blocks
            y = 0
            while y < self.height:
            
                x = 0
                while x < self.width:
                
                    bit_offset = self.read_block(bit_offset, x, y)
                    x += 4
                
                y += 4
        
        return bit_offset
