import struct
import Image
import replay

def pack_yuv(Y, U, V):

    d = ""
    for y, u, v in zip(Y, U, V):
        d += struct.pack("<H", y | (u << 5) | (v << 10))
    
    return d

def frame_to_image(width, height, yuv_data):

    rgb = replay.yuv555_to_rgb(yuv_data)
    
    return Image.frombytes("RGBA", (width, height), rgb)
